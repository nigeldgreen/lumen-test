<?php

use App\Video;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class VideoMetadataTest extends TestCase
{
    /**
     * Make sure the endpoint is only available under POST
     *
     * @return void
     */
    public function testFailureOnGetRequest()
    {
        $this->json('GET', '/video/v1')
             ->seeJson([
                'status' => "Method not supported",
             ]);
    }

    /**
     * An exception is raised if the file is not found
     *
     * @return void
     */
    public function testExceptionIfNoFileFoundAtLocation()
    {
        $this->expectException(InvalidArgumentException::class);
        $file = env('STORAGE_PATH') . "fileNotHere.mp4";
        $video = new Video($file);
    }

    /**
     * We can create a video object and access the file passed in
     *
     * @return void
     */
    public function testVideoPathIsSetForValidFile()
    {
        $file = "small.webm";
        $video = new Video($file);
        $this->assertEquals($video->file(), env('STORAGE_PATH') . $file);
    }

    /**
     * Call the POST route and check we get an error if the file is missing
     *
     * @return void
     */
    public function testMetadataReturnedForValidFile()
    {
        $this->json('POST', '/video/v1')
             ->seeJson([
                'status' => 'File missing'
            ]);
    }
}
