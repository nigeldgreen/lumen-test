<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/video/v1', function ()    {
    return response()->json(['status' => 'Method not supported']);
});

$router->post('/video/v1', 'VideoMetadataController@create');
