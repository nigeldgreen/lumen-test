<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;

class VideoMetadataController extends Controller
{
    /**
     * Creates a new controller to extract and return file metatdata
     *
     * @return Illuminate\Support\Facades\Response
     */
    public function create(Request $request)
    {
        if ($request->hasFile('video') && $request->file('video')->isValid()) {
            $videoUpload = $request->file('video');
            $videoUpload->move(env('STORAGE_PATH'), $videoUpload);

            // Using the name and extension passed in for the demo here. Would implement a
            // whitelist to validate the extension and sanitize the name in a live environment
            $video = new Video($videoUpload->getClientOriginalName() . $videoUpload->getClientOriginalExtension());
            
            return response()->json([
                'status' => 'OK',
                'data' => $video->getMetadata()
            ]);
        } else {
            return response()->json(['status' => 'File missing']);
        }
    }
}
