<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $file;

    /**
     * Constructor for the Video class
     *
     * Take in a filename and store it as an instance variable if it exists 
     * 
     * @return void
     */
    public function __construct($file)
    {
        $file = env('STORAGE_PATH') . $file;
        if (! file_exists($file)) {
            throw new \InvalidArgumentException("File not found");
        } else {
            $this->file = $file;
        }
    }

    /**
     * Return the metadata for the file as read by getId3
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function getMetadata()
    {
        require_once('vendor/james-heinrich/getid3/getid3/getid3.php');
        $getID3 = new \getID3;

        return $getID3->analyze($this->file);
    }

    /**
     * Return the $file instance variable
     *
     * @return string
     */
    public function file()
    {
        return $this->file;
    }
}
