# PHP Lumen Test - Nigel Green
Really enjoyed working through the test and it took just over 2 hours in total. Overall I'm pretty happy with the results - I would have liked a test that fully tested the request to the application with a file but I'm not familiar enough with the testing in Lumen to see how to do that. That said, I'm happy that the only thing really untested is moving the file itself to the new location so I'm pretty confident overall.

A few things that occurred to me as I was working through, thinking what I would do differently if this was for production-ready code within the business rather than a test in isolation:

* Find out what else might be required for the video metadata endpoints. I created a controller and a Video class rather than just work in a route closure to keep things separated and extendable, but this may need to be refactored as extra requirements are discovered.
* I added the path to local storage as an environment variable so it can be set for each server as required. Laravel has quite a good storage setup but this appears to have gone out of the Lumen docs - I'd like to make this a bit more robust.
* I've not added any checks on the file apart from that it exists and is valid. Business requirements might mean we needed to extend the checks out: check file size, check against a whitelist of file types, check the origin of the file if we have different requirements for different clients.
* I've treated the file as clean data but I'd want to understand a bit more about what is acceptable and set up a whitelist that we could use to sanitize the input (as far as is possible). May also need to do a MIME type check to see that the file we've got is what it is reported to be...
* Would need to make sure CORS headers are set on the server so we are able to access files in this way from all the required endpoints (clients may want to send files via AJAX).

Thanks for the opportunity - would welcome any feedback!!

Nigel
